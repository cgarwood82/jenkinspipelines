pipeline {
  agent any 
  stages {

    stage("Get hosts") {
      steps {
        checkout changelog: false,
	poll: false,
	scm: [
	    $class: 'GitSCM', 
	    branches: [[name: '*/master']], 
	    doGenerateSubmoduleConfigurations: false, 
	    extensions: [[$class: 'WipeWorkspace']], 
	    submoduleCfg: [], 
	    userRemoteConfigs: [[credentialsId: 'cgarwood82', url: 'git@bitbucket.org:cgarwood82/jenkinspipelinehosts.git']]
	]
      }
    }

    stage("Ping Check") {
      steps {
 	wrap([$class: 'ChefIdentityBuildWrapper', jobIdentity: 'my-chef']) {
	  script {
	    readFile("${env.WORKSPACE}/hosts/patch.plan").split('\n').each { line ->
              sh returnStdout: true, script: """
	        #!/usr/bin/env bash 

		# We set +e so the job doesn't bail if the exit != 0
		set +e

		# Send a single ping to check to the host
		ping -c1 -W3 ${line}
		ping_exit=\$?
		
		# We set it back to fail on != 0
		set -e

		if [[ \$ping_exit -ne 0 ]]; then
		  echo "\${line} is down\nRemoving from patch.plan\"
		  sed -i '/${line}/d' ${env.WORKSPACE}/hosts/patch.plan
		  echo ${line} >> ${env.WORKSPACE}/hosts/ping.fail
		fi
	      """.stripIndent()
	    }
	  }
	}
      }
    }

    stage("Verify Server is chef'ed") {
      steps {
 	wrap([$class: 'ChefIdentityBuildWrapper', jobIdentity: 'my-chef']) {
	  script {
	    readFile("${env.WORKSPACE}/hosts/patch.plan").split('\n').each { line ->
              sh returnStdout: true, script: """
	        #!/usr/bin/env bash 
	        
		# chef related
		knife ssl fetch &> /dev/null
		
		# We set +e so the job doesn't bail if the exit != 0
		set +e

		knife ssh -t4 -e name:${line} \"uptime\"
		chef_exit=\$?
		
		# We set it back to fail on != 0
		set -e

		if [[ \$chef_exit -ne 0 ]]; then
		  echo "\${line} is down\nRemoving from patch.plan\"
		  sed -i '/${line}/d' ${env.WORKSPACE}/hosts/patch.plan
		  echo ${line} >> ${env.WORKSPACE}/hosts/chef.fail
		fi
	      """.stripIndent()
	    }
	  }
	}
      }
    }

    stage("Create release branch") {
      steps {
        sshagent(['cgarwood82']) {
	  sh returnStdout: true, script: """
	  #!/usr/bin/env bash

	  #add and commit changes
	  git add hosts/*
	  git commit -m "Jenkins job"

	  #git push
	  git push origin HEAD:master 
	  """.stripIndent()
	}
      }
    }

  }
}
