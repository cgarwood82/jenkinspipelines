# Jenkins Pipelines
This is a collection of of jenkins pipelines that I use in my
lab to do various things. Mostly operational stuff that I do at
work, but also to work on proof of concept stuff in my free
time. 

## Chef Plugin Notice
I make use of the chef-identity-plugin, among others, which is
available in jenkins plugins repo BUT it's near ancient and
doesn't support pipelines by itself. So you need to grab the 
source and build it yourself and upload the resulting plugin
to your jenkins master. 
